# xzoom.js 图片放大镜

## 插件截图
![插件截图](https://images.gitee.com/uploads/images/2021/0606/092822_5551c029_1402003.jpeg "插件截图")

## 使用方法
引入 jquery.js 及 xzoom.js

### html
```
<div class="xzoom">
  <div class="xzoom_show"><img src="images/01_lit.jpg" data-xzoom="images/01.jpg" /></div>

  <div class="xzoom_thumb">
    <div class="xzoom_thumbin">
      <ul>
        <li><img src="images/01_lit.jpg" data-xzoom="images/01.jpg" /></li>
        <li><img src="images/02_lit.jpg" data-xzoom="images/02.jpg" /></li>
        <li><img src="images/03_lit.jpg" data-xzoom="images/03.jpg" /></li>
        <li><img src="images/04_lit.jpg" data-xzoom="images/04.jpg" /></li>
        <li><img src="images/05_lit.jpg" data-xzoom="images/05.jpg" /></li>
      </ul>
    </div>
    <div class="xzoom_prev"></div>
    <div class="xzoom_next"></div>
  </div>
</div>
```
#### _data-xzoom 属性为大图路径，可选，如果没有则读取 src_

### js
```
<script>
$('.xzoom').xzoom({
  thumbs: 4,
  scroll: true,
});
</script>
```

### 参数说明
| 参数名    | 说明                       | 默认    |
|-----------|--------------------------|-------|
| width     | 展示区图片宽度            | 500   |
| height    | 展示区图片高度            | 500   |
| zwidth    | 放大框宽度。不能设置高度，高度由图片本身决定的 | 600   |
| zoom      | 放大倍数。这里的倍数是根据 zwidth 决定的，例如 zwidth=600，zoom=3，则实际的放大图宽度为 1800px，高度自适应 | 3     |
| mask      | 鼠标遮罩大小                   | 100   |
| scroll    | 是否卷轴显示                   | false |
| thumbs    | 一行显示缩略图数量                | 4     |
| thumbType | 缩略图操作方式。hover \| click | 'hover' |
| thumbPad  | 缩略图间距                    | 5     |
| thumbBor  | 缩略图选中时边框颜色             | ''     |

### 尺寸参数图解
![尺寸参数图解](https://images.gitee.com/uploads/images/2021/0605/225349_087198ed_1402003.jpeg "尺寸参数图解")
